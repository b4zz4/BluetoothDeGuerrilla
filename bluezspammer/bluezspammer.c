/*

Copyright (C) 2006-2009  Alberto Moreno Tablado

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation;

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY
RIGHTS.  IN NO EVENT SHALL THE COPYRIGHT HOLDER(S) AND AUTHOR(S) BE LIABLE
FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY
DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

ALL LIABILITY, INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PATENTS,
COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS, RELATING TO USE OF THIS SOFTWARE
IS DISCLAIMED.

*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

#include "oui.h"

void hex2bin(char *hex, char *bin);
int get_obex_channel(char *bt_address);
static int get_channel_from_service_desc(sdp_data_t *value, void *user);
char *getFabricante(char *mac_code);
char *getClass(char *binclass);

//Variables globales
int banderaFiltro = 0;
int banderaEnvio = 0;

void usage(char *programa)
{
	printf(
	"\nUsage: %s [-p file] [-f] [-h]\n"
	"\t               discover devices only\n"
	"\t-p [file]      discover devices and send files\n"
	"\t-f             Phone Major Class devices only\n"
	"\t-h             help\n\n",programa);
}

int main (int argc, char **argv)
{
      inquiry_info *ii = NULL; //Almacena la lista de dispositivos detectados durante el inquiry 
      int max_rsp, num_rsp; //Numero de respuestas/dispositivos detectados
      int dev_id; //Identificador del adaptador Bluetooth local
      int socket; //Socket HCI;
      int len, i, j, k;
      int args;

      int canal_obex_object_push;
      char comando_obex[40];
      char objeto[20]="owned.jpg";

      char MAC_dev[20]; //Direccion MAC del dispositivo detectado
      char MAC_code[7]; //Codigo de 3 primeros bytes de la direccion MAC
      char nombre_dev[248]; //Nombre del dispositivo detectado
      char class_dev[7]; //Clase del dispositivo detectado

      uint8_t hexclass[3]; //Estructura para almacenar el campo device_class
      char binclass[24]; //Almacena el campo device_class en formato binario
      char rev_binclass[24]; //Almacena el campo device_class en formato binario (big endian)

      printf("BlueZSpammer, an obexftp front-end tool for proximity marketing.\n");

      if (argc < 2) 
      {
	    banderaEnvio = 0;
      }

      while ((args = getopt (argc, argv, "+p:hf")) != -1)
      {
            switch (args)
            {
		  case 'p':		//Indica el objeto a enviar
			strcpy(objeto,optarg);
			banderaEnvio = 1;
			break;

		  case 'f':		//Demo, solo descubre dispositivos
                  	banderaFiltro = 1;
			break;

                  case 'h':		//help / ayuda
                  	usage(argv[0]);
			return (0);
            }
      }

      //Obtenemos el identificador del adaptador local Bluetooth
      dev_id = hci_get_route(NULL);
      if (dev_id < 0)
      {
            printf("[!] Error. No Bluetooth device installed.\n"); 
            exit(1);
      }
                      
      //Abrimos un socket local HCI
      socket = hci_open_dev(dev_id);
      if (socket < 0)
      {
            printf("[!] Error. Can't open HCI socket.\n");
            exit(1);
      }

      //Inicializamos algunas variables
      len = 8; //El tiempo de inquiry por dispositivo es de 1.28x8=10.24 secs/dispositivo
      max_rsp = 255; //Se pueden detectar a lo sumo 255 dispositivos

      //Creamos la lista de dispositivos detectados con hci_inquiry
      ii = (inquiry_info*)malloc(max_rsp * sizeof(inquiry_info));

      if(!banderaFiltro)
		printf("\nDiscovering devices...\n");
      else
		printf("\nDiscovering devices (only Phone Class devices)...\n");

      printf("\nBDADDR              CLASS OF DEVICE\t\tNAME (CHIPSET VENDOR)\n");

      //hci_inquiry lleva a cabo un descubrimiento de dispositivos Bluetooth y devuelve una lista de
      //dispositivos detectados en inquiry_info ii para ser almacenados.
      //La bandera IREQ_CACHE_FLUSH permite que la caché sea limpiada antes de buscar nuevos dispositivos.
      //En otro caso, podrian aparecer dispositivos anteriormente detectados pero ahora fuera de rango.
      num_rsp = hci_inquiry(dev_id, len, max_rsp, NULL, &ii, IREQ_CACHE_FLUSH);
      if(num_rsp < 0)
            printf("[!] Error. Can't attemp hci_inquiry.\n");

      //Para cada una de las respuestas obtenidas durante el inquiry obtenemos el nombre del dispositivo
      for(i=0;i<num_rsp;i++)
      {
	    memset(MAC_dev, 0, sizeof(MAC_dev));
            ba2str(&(ii+i)->bdaddr, MAC_dev);
            memset(nombre_dev, 0, sizeof(nombre_dev));
            if(hci_read_remote_name(socket, &(ii+i)->bdaddr, sizeof(nombre_dev), nombre_dev, 0) < 0)
                  strcpy(nombre_dev, "[Unknown]");

	    //Obtenemos el nombre del fabricante del Chip Bluetooth a partir del analisis de su direccion MAC
	    sprintf(MAC_code, "%c%c%c%c%c%c", MAC_dev[0],MAC_dev[1],MAC_dev[3],MAC_dev[4],MAC_dev[6],MAC_dev[7]);

	    //Extraemos el device class de la estructura ii
            memcpy(hexclass, (ii+i)->dev_class, 3);
            sprintf(class_dev, "%2.2x%2.2x%2.2x", hexclass[2], hexclass[1], hexclass[0]);
            hex2bin(class_dev, binclass); //Convertimos dev_class

            //Ordenamos el campo device_class binario en formato big endian 11010 -> 01011
            k=23;
            for(j=0;j<=23;j++)
            {
               	rev_binclass[k]=binclass[j];
               	k--;
            }

	    memset(MAC_dev, 0, sizeof(MAC_dev));
            ba2str(&(ii+i)->bdaddr, MAC_dev);

	    //BlueZSpammer tiene como objetivo Telefonos Moviles y Smart Phones
	    if((rev_binclass[12]=='0') && (rev_binclass[11]=='0') && (rev_binclass[10]=='0') && (rev_binclass[9]=='1') && (rev_binclass[8]=='0'))
	    {
            	//Minor Device Classes (bits 2-7) - Phone Major Class
		if((rev_binclass[4]=='0') && (rev_binclass[3]=='0') && (rev_binclass[2]=='1'))
			printf("\n%s   Phone > Cellular\t\t%s (%s)", MAC_dev, nombre_dev, getFabricante(MAC_code));
            	if((rev_binclass[4]=='0') && (rev_binclass[3]=='1') && (rev_binclass[2]=='1'))
			printf("\n%s   Phone > Smartphone\t\t%s (%s)", MAC_dev, nombre_dev, getFabricante(MAC_code));

		//Obtenemos el canal RFCOMM asociado al perfil OBEX Object Push
	    	canal_obex_object_push = get_obex_channel(MAC_dev);

	    	if(banderaEnvio)
	    	{
			printf("\n\nSending file...\n");
    			sprintf(comando_obex,"obexftp --nopath --noconn --uuid none -b %s -B %d -p %s", MAC_dev, canal_obex_object_push, objeto);
			system(comando_obex);
	    	}
		
	    }
	    else if (!banderaFiltro)
	    {
		printf("\n%s   %s\t\t%s (%s)", MAC_dev, getClass(rev_binclass), nombre_dev, getFabricante(MAC_code));

		//Obtenemos el canal RFCOMM asociado al perfil OBEX Object Push
	    	canal_obex_object_push = get_obex_channel(MAC_dev);

	    	if(banderaEnvio)
	    	{
			printf("\n\nSending file...\n");
    			sprintf(comando_obex,"obexftp --nopath --noconn --uuid none -b %s -B %d -p %s", MAC_dev, canal_obex_object_push, objeto);
			system(comando_obex);
	    	}
	     }	   

      }
      
      free(ii);

      close(socket);

      printf("\n\n");

      return(0);

}

//Funcion que obtiene la descripcion del fabricante de un determinado codigo MAC
char *getFabricante(char *mac_code) 
{
	int i;

   	for (i=0; (strncmp(ListaFabricantes[i].codigoMAC, "EOF", 3)!=0); i++) 
	{
      		if (strncmp(ListaFabricantes[i].codigoMAC, mac_code, 6)==0) 
		{
			return(ListaFabricantes[i].fabricante);
		}
   	}

   	return("Unknown");
}

//Funcion que analiza el campo device_class en binario y devuelve la informacion obtenida
char *getClass (char *binclass)
{
      //Major Device Classes (bits 8-12)
      if((binclass[12]=='0') && (binclass[11]=='0') && (binclass[10]=='0') && (binclass[9]=='0') && (binclass[8]=='0'))
            return("Miscelaneous");

      if((binclass[12]=='0') && (binclass[11]=='0') && (binclass[10]=='0') && (binclass[9]=='0') && (binclass[8]=='1'))
      {
            //Minor Device Classes (bits 2-7) - Computer Major Class
            if((binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Computer > Uncategorized");
            if((binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Computer > Workstation");
            if((binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='0'))
                  return("Computer > Server");
            if((binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='1'))
                  return("Computer > Laptop");
            if((binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Computer > PDA");
            if((binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Computer > PDA");
            if((binclass[4]=='1') && (binclass[3]=='1') && (binclass[2]=='0'))
                  return("Computer > Wearable computer");
      }

      if((binclass[12]=='0') && (binclass[11]=='0') && (binclass[10]=='0') && (binclass[9]=='1') && (binclass[8]=='0'))
      {
            //Minor Device Classes (bits 2-7) - Phone Major Class
            if((binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Phone > Uncategorized");
            if((binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Phone > Cellular");
            if((binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='0'))
                  return("Phone > Cordless");
            if((binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='1'))
                  return("Phone > Smartphone");
            if((binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Phone > Wired modem");
            if((binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Phone > ISDN Access");
      }

      if((binclass[12]=='0') && (binclass[11]=='0') && (binclass[10]=='0') && (binclass[9]=='1') && (binclass[8]=='1'))
            return("LAN / Network Access point");

      if((binclass[12]=='0') && (binclass[11]=='0') && (binclass[10]=='1') && (binclass[9]=='0') && (binclass[8]=='0'))
      {
            //Minor Device Classes (bits 2-7) - Audio/Video Major Class
            if((binclass[6]=='0') && (binclass[5]=='0') && (binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Audio > Uncategorized");
            if((binclass[6]=='0') && (binclass[5]=='0') && (binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Audio > Wearable Headset");
            if((binclass[6]=='0') && (binclass[5]=='0') && (binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='0'))
                  return("Audio > Hands-free");
            if((binclass[6]=='0') && (binclass[5]=='0') && (binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='1'))
                  return("Audio > Hands-free");
            if((binclass[6]=='0') && (binclass[5]=='0') && (binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Audio > Microphone");
            if((binclass[6]=='0') && (binclass[5]=='0') && (binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Audio > Loudspeaker");
            if((binclass[6]=='0') && (binclass[5]=='0') && (binclass[4]=='1') && (binclass[3]=='1') && (binclass[2]=='0'))
                  return("Audio > Headphones");
            if((binclass[6]=='0') && (binclass[5]=='0') && (binclass[4]=='1') && (binclass[3]=='1') && (binclass[2]=='1'))
                  return("Audio > Portable Audio");
            if((binclass[6]=='0') && (binclass[5]=='1') && (binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Audio > Car audio");
            if((binclass[6]=='0') && (binclass[5]=='1') && (binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Audio > Set-top box");
            if((binclass[6]=='0') && (binclass[5]=='1') && (binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='0'))
                  return("Audio > HiFi Device");
            if((binclass[6]=='0') && (binclass[5]=='1') && (binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='1'))
                  return("Video > VCR");
            if((binclass[6]=='0') && (binclass[5]=='1') && (binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Video > Video Camera");
            if((binclass[6]=='0') && (binclass[5]=='1') && (binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Video > Camcorder");
            if((binclass[6]=='0') && (binclass[5]=='1') && (binclass[4]=='1') && (binclass[3]=='1') && (binclass[2]=='0'))
                  return("Video > Video Monitor");
            if((binclass[6]=='0') && (binclass[5]=='1') && (binclass[4]=='1') && (binclass[3]=='1') && (binclass[2]=='1'))
                  return("Video > Video Display");
            if((binclass[6]=='1') && (binclass[5]=='0') && (binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Video > Conferencing");
            if((binclass[6]=='1') && (binclass[5]=='0') && (binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Video > (Reserved)");
            if((binclass[6]=='1') && (binclass[5]=='0') && (binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='0'))
                  return("Video > Gaming");
      }

      if((binclass[12]=='0') && (binclass[11]=='0') && (binclass[10]=='1') && (binclass[9]=='0') && (binclass[8]=='1'))
      {
            //Minor Device Classes (bits 2-7) - Peripheral Major Class
            if((binclass[7]=='0') && (binclass[6]=='0'))
                  return("Peripheral > Uncategorized");
            if((binclass[7]=='0') && (binclass[6]=='1'))
                  return("Peripheral > Keyboard");
            if((binclass[7]=='1') && (binclass[6]=='0'))
                  return("Peripheral > Pointing");
            if((binclass[7]=='1') && (binclass[6]=='1'))
                  return("Peripheral > Combo");
      }

      if((binclass[12]=='0') && (binclass[11]=='0') && (binclass[10]=='1') && (binclass[9]=='1') && (binclass[8]=='0'))
      {
            //Minor Device Classes (bits 2-7) - Imaging Major Class
            if(binclass[7]=='1')
                  return("Imaging > Display");
            if(binclass[6]=='1')
                  return("Imaging > Scanner");
            if(binclass[5]=='1')
                  return("Imaging > Camera");
            if(binclass[4]=='1')
                  return("Imaging > Display");
      }

      if((binclass[12]=='0') && (binclass[11]=='0') && (binclass[10]=='1') && (binclass[9]=='1') && (binclass[8]=='1'))
      {
            //Minor Device Classes (bits 2-7) - Wereable Major Class
            if((binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Wearable > Uncategorized");
            if((binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Wearable > Wrist Watch");
            if((binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='0'))
                  return("Wearable > Pager");
            if((binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='1'))
                  return("Wearable > Jacket");
            if((binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Wearable > Helmet");
            if((binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Wearable > Glasses");
      }

      if((binclass[12]=='0') && (binclass[11]=='1') && (binclass[10]=='0') && (binclass[9]=='0') && (binclass[8]=='0'))
      {
            //Minor Device Classes (bits 2-7) - Toy Major Class
            if((binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Toy > Uncategorized");
            if((binclass[4]=='0') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Toy > Robot");
            if((binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='0'))
                  return("Toy > Vehicle");
            if((binclass[4]=='0') && (binclass[3]=='1') && (binclass[2]=='1'))
                  return("Toy > Action Figure");
            if((binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='0'))
                  return("Toy > Controller");
            if((binclass[4]=='1') && (binclass[3]=='0') && (binclass[2]=='1'))
                  return("Toy > Game");
      }

      if((binclass[12]=='1') && (binclass[11]=='1') && (binclass[10]=='1') && (binclass[9]=='1') && (binclass[8]=='1'))
            return("Uncategorized");

      return("Uncategorized");    
}


//Funcion que obtiene el canal RFCOMM utilizado por el perfil OBEX Object Push en el dispositivo
int get_obex_channel(char *bt_address)
{

     bdaddr_t bdaddr;
     uint16_t class = 0x1105; 
     sdp_list_t *attrid, *search, *seq, *next;
     uint32_t range = 0x0000ffff;
     char str[20];
     sdp_session_t *sess;
     uint32_t channel = -1;
     uuid_t   group;      /* Browse group */
     bdaddr_t interface;
 
     str2ba(bt_address, &bdaddr);
     sdp_uuid16_create(&group, class);
     
     bacpy(&interface, BDADDR_ANY);
     sess = sdp_connect(&interface, &bdaddr, SDP_RETRY_IF_BUSY);
     ba2str(&bdaddr, str);
     if (!sess) {
         return -1;
     }
 
     attrid = sdp_list_append(0, &range);
     search = sdp_list_append(0, &group);
     if (sdp_service_search_attr_req(sess, search, SDP_ATTR_REQ_RANGE, attrid, &seq)) {
         sdp_close(sess);
         return -1;
     }
     sdp_list_free(attrid, 0);
     sdp_list_free(search, 0);
 
     for (; seq; seq = next) {
         sdp_record_t *rec = (sdp_record_t *) seq->data;
         sdp_list_t *proto = 0;
         if (sdp_get_access_protos(rec, &proto) == 0) {
             sdp_list_t* ptr = proto;
             for(;ptr != NULL;ptr = ptr->next){
                 sdp_list_t *protDescSeq = (sdp_list_t *)ptr->data;
                 for(;protDescSeq != NULL;protDescSeq = protDescSeq->next){
                     channel = get_channel_from_service_desc(protDescSeq->data, NULL);
                     if(channel != -1) break;
                 } 
             } 
             sdp_list_free(proto, 0);
         }
 
         next = seq->next;
         free(seq);
         sdp_record_free(rec);
     }
 
     sdp_close(sess);
     return channel;
}
 
 
static int get_channel_from_service_desc(sdp_data_t *value, void *user)
{
    char str[MAX_LEN_PROTOCOL_UUID_STR];
    char UUID_str[MAX_LEN_UUID_STR];

    sdp_data_t *p = (sdp_data_t *)value;
    int i = 0, proto = 0;
     
    for (; p; p = p->next, i++) {
        switch (p->dtd) {
        case SDP_UUID16:
        case SDP_UUID32:
        case SDP_UUID128:
            sdp_uuid2strn(&p->val.uuid, UUID_str, MAX_LEN_UUID_STR);
            sdp_proto_uuid2strn(&p->val.uuid, str, sizeof(str));
            proto = sdp_uuid_to_proto(&p->val.uuid);
            break;
        case SDP_UINT8:
            if (proto == RFCOMM_UUID){
                return p->val.uint8;
            }
            break;
        case SDP_UINT16:
        case SDP_SEQ16:
        case SDP_SEQ8:
        default:
            break;
        }
    }
    return -1;
}

//Funcion que convierte un string de hexadecimal a binario
void hex2bin(char *hex, char *bin)
{
      int i;
      strcpy(bin,"");

      for(i=0;i<=5;i++)
      {
            if(hex[i] == '0')
                  strcat(bin,"0000");
            if(hex[i] == '1')
                  strcat(bin,"0001");
            if(hex[i] == '2')
                  strcat(bin,"0010");
            if(hex[i] == '3')
                  strcat(bin,"0011");
            if(hex[i] == '4')
                  strcat(bin,"0100");
            if(hex[i] == '5')
                  strcat(bin,"0101");
            if(hex[i] == '6')
                  strcat(bin,"0110");
            if(hex[i] == '7')
                  strcat(bin,"0111");
            if(hex[i] == '8')
                  strcat(bin,"1000");
            if(hex[i] == '9')
                  strcat(bin,"1001");
            if(hex[i] == 'a')
                  strcat(bin,"1010");
            if(hex[i] == 'b')
                  strcat(bin,"1011");
            if(hex[i] == 'c')
                  strcat(bin,"1100");
            if(hex[i] == 'd')
                  strcat(bin,"1101");
            if(hex[i] == 'e')
                  strcat(bin,"1110");
            if(hex[i] == 'f')
                  strcat(bin,"1111");
      }
}
