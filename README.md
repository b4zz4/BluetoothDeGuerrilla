# Bluetooth de Guerrilla

Método para difundir problemáticas, hacer anuncios y publicidad en por **bluetooth**.
La idea es poder difundir problemáticas, eventos y protestas por **bluetooth** llegando directamente a los dispositivos celulares de la gente que pasa cerca del lugar.

## Instalación
    
    sudo aptitude install bluez-utils bluez-gnome libbluetooth-dev libopenobex1 openobex-apps obexftp
    cd /tmp
    wget https://github.com/b4zz4/BluetoothDeGuerrilla/archive/master.zip
    unzip master.zip
    cd BluetoothDeGuerrilla-master/bluezspammer
    gcc -lbluetooth bluezspammer.c -o bluezspammer
    make
    sudo cp bluezspammer /usr/bin 
    cd ..
    sudo cp guerrillabluetooth /usr/bin 

### Prueba

    bluezspammer -p imagen.png

## Usar

Podemos guardar este archivo en un carpeta con imágenes y poner:

    guerrillabluetooth

**Nota:** Lo recomendable es generar archivos pequeños no más de 128 píxeles y que el nombre pueda enviar un mensaje, así si el archivo no es descargado el mensaje llega igual :P

### hacer campaña con un texto

    guerrillabluetooth '¡¡Hoy Fiesta!!'

### hacer campaña con múltiples textos

    guerrillabluetooth '¡¡Hoy Fiesta!!,Aristobulo del Valle 1851'

## Fuentes

-   [Publicidad spam por bluetooth](http://linuxparanabos.blogspot.com.ar/2008/04/publicidad-spam-por-bluetooth.html)



